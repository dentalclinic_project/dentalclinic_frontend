import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type Patient from '@/types/Patient'
import patientService from '@/services/patient'
// import router from "@/router";

export const usePatientStore = defineStore('patient', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const keyword = ref('')
  const messageStore = useMessageStore()
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getPatients()
  })
  const editedPatient = ref<Patient>({
    patien_id: 0,
    patien_name: '',
    patien_surname: '',
    patien_cardnumber: '',
    patien_birth: '',
    patien_phonenumber: '',
    patien_age: 0,
    patien_gender: '',
    patien_address: '',
    patien_drugAllergy: '',
    patien_congenitalDisease: ''
  })

  const patients = ref<Patient[]>([])

  async function getPatient(search: string) {
    try {
      const res = await patientService.getPat({ search: search })
      patients.value = res.data
      console.log('ดึงข้อมูล Patient')
    } catch (e) {
      console.log(e)
    }
  }

  async function getPatients() {
    try {
      const res = await patientService.getPatients()
      patients.value = res.data
      console.log(patients.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Patient ได้')
    }
  }

  async function deletePatient(id: number) {
    try {
      const res = await patientService.deletePatient(id)
      await getPatients()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Patient ได้')
    }
  }

  async function savePatient() {
    try {
      if (editedPatient.value.patien_id) {
        const res = await patientService.updatePatient(
          editedPatient.value.patien_id,
          editedPatient.value
        )
      } else {
        const res = await patientService.savePatient(editedPatient.value)
      }

      dialog.value = false
      clearPatient()
      await getPatients()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Patient ได้')
    }
  }

  const editPatient = (patient: Patient) => {
    editedPatient.value = { ...patient }
    dialog.value = true
  }

  function clearPatient() {
    editedPatient.value = {
      patien_name: '',
      patien_surname: '',
      patien_cardnumber: '',
      patien_birth: '',
      patien_phonenumber: '',
      patien_age: 0,
      patien_gender: '',
      patien_address: '',
      patien_drugAllergy: '',
      patien_congenitalDisease: ''
    }
  }

  return {
    patients,
    getPatients,
    deletePatient,
    savePatient,
    editPatient,
    editedPatient,
    clearPatient,
    getPatient,
    keyword,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import medicineService from '@/services/medicine'
import type Medicine from '@/types/Medicine'
// import router from "@/router";

export const useMedicineStore = defineStore('medicine', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedMedicine = ref<Medicine>({
    medicine_id: 0,
    medicine_name: '',
    medicine_remaining: 0,
    medicine_cost: 0,
    medicine_sell: 0,
    medicine_medicineType: '',
    medicine_status: '',
    medicineTypeId: 0
  })

  const medicines = ref<Medicine[]>([])

  async function getMedicines() {
    try {
      const res = await medicineService.getMedicines()
      medicines.value = res.data
      console.log(medicines.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Medicine ได้')
    }
  }

  async function deleteMedicine(id: number) {
    try {
      const res = await medicineService.deleteMedicine(id)
      await getMedicines()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Medicine ได้')
    }
  }

  async function saveMedicine() {
    try {
      if (editedMedicine.value.medicine_id) {
        const res = await medicineService.updateMedicine(
          editedMedicine.value.medicine_id,
          editedMedicine.value
        )
      } else {
        const res = await medicineService.saveMedicine(editedMedicine.value)
      }

      dialog.value = false
      clearMedicine()
      await getMedicines()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Medicine ได้')
    }
  }

  const editMedicine = (medicine: Medicine) => {
    editedMedicine.value = { ...medicine }
    dialog.value = true
  }

  function clearMedicine() {
    editedMedicine.value = {
      medicine_id: 0,
      medicine_name: '',
      medicine_remaining: 0,
      medicine_cost: 0,
      medicine_sell: 0,
      medicine_medicineType: '',
      medicine_status: '',
      medicineTypeId: 0
    }
  }

  return {
    medicines,
    getMedicines,
    deleteMedicine,
    saveMedicine,
    editMedicine,
    editedMedicine,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

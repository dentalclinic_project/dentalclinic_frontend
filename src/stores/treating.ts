import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type Treating from '@/types/Treating'
import treatingService from '@/services/treating'
// import router from "@/router";

export const useTreatingStore = defineStore('treating', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedTreating = ref<Treating>({
    his_id: 0,
    his_patien: '',
    his_doctor: '',
    patienId: 0,
    doctorId: 0,
    treatmenttypeId: 0,
    treatmentDetailId: 0,
    precribingDetailId: 0
  })

  const treatings = ref<Treating[]>([])

  async function getTreatings() {
    try {
      const res = await treatingService.getTreatings()
      treatings.value = res.data
      console.log(treatings.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Treating ได้')
    }
  }

  async function deleteTreating(id: number) {
    try {
      const res = await treatingService.deleteTreating(id)
      await getTreatings()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Treating ได้')
    }
  }

  async function saveTreating() {
    try {
      if (editedTreating.value.his_id) {
        const res = await treatingService.updateTreating(
          editedTreating.value.his_id,
          editedTreating.value
        )
      } else {
        const res = await treatingService.saveTreating(editedTreating.value)
      }

      dialog.value = false
      clearTreating()
      await getTreatings()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Treating ได้')
    }
  }

  const editTreating = (treating: Treating) => {
    editedTreating.value = { ...treating }
    dialog.value = true
  }

  function clearTreating() {
    editedTreating.value = {
      his_id: 0,
      his_patien: '',
      his_doctor: '',
      patienId: 0,
      doctorId: 0,
      treatmenttypeId: 0,
      treatmentDetailId: 0,
      precribingDetailId: 0
    }
  }

  return {
    treatings,
    getTreatings,
    deleteTreating,
    saveTreating,
    editTreating,
    editedTreating,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

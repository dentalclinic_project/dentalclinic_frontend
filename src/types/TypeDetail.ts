export default interface TypeDetail {
  typeDetail_id: number
  typeDetail_service: string
  typeDetail_unitService: string
  typeDetail_price: number
  treatmentType: number
}

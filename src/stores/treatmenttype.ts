import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import treatmenttypeService from '@/services/treatmenttype'
import type TreatmentType from '@/types/TreatmentType'
// import router from "@/router";

export const useTreatmentTypeStore = defineStore('treatmenttype', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedTreatmentType = ref<TreatmentType>({
    treatmenttype_id: 0,
    treatmenttype_name: ''
  })

  const treatmenttypes = ref<TreatmentType[]>([])

  async function getTreatmentTypes() {
    try {
      const res = await treatmenttypeService.getTreatmentTypes()
      treatmenttypes.value = res.data
      console.log(treatmenttypes.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentType ได้')
    }
  }

  async function deleteTreatmentType(id: number) {
    try {
      const res = await treatmenttypeService.deleteTreatmentType(id)
      await getTreatmentTypes()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentType ได้')
    }
  }

  async function saveTreatmentType() {
    try {
      if (editedTreatmentType.value.treatmenttype_id) {
        const res = await treatmenttypeService.updateTreatmentType(
          editedTreatmentType.value.treatmenttype_id,
          editedTreatmentType.value
        )
      } else {
        const res = await treatmenttypeService.saveTreatmentType(editedTreatmentType.value)
      }

      dialog.value = false
      clearTreatmentType()
      await getTreatmentTypes()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentType ได้')
    }
  }

  const editTreatmentType = (treatmenttype: TreatmentType) => {
    editedTreatmentType.value = { ...treatmenttype }
    dialog.value = true
  }

  function clearTreatmentType() {
    editedTreatmentType.value = {
      treatmenttype_id: 0,
      treatmenttype_name: ''
    }
  }

  return {
    treatmenttypes,
    getTreatmentTypes,
    deleteTreatmentType,
    saveTreatmentType,
    editTreatmentType,
    editedTreatmentType,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

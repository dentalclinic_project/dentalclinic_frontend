import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import typedetailService from '@/services/typedetail'
import type TypeDetail from '@/types/TypeDetail'
// import router from "@/router";

export const useTypeDetailStore = defineStore('typedetail', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedTypeDetail = ref<TypeDetail>({
    typeDetail_id: 0,
    typeDetail_service: '',
    typeDetail_unitService: '',
    typeDetail_price: 0,
    treatmentType: 0
  })

  const typedetails = ref<TypeDetail[]>([])

  async function getTypeDetails() {
    try {
      const res = await typedetailService.getTypeDetails()
      typedetails.value = res.data
      console.log(typedetails.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TypeDetail ได้')
    }
  }

  async function deleteTypeDetail(id: number) {
    try {
      const res = await typedetailService.deleteTypeDetail(id)
      await getTypeDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TypeDetail ได้')
    }
  }

  async function saveTypeDetail() {
    try {
      if (editedTypeDetail.value.typeDetail_id) {
        const res = await typedetailService.updateTypeDetail(
          editedTypeDetail.value.typeDetail_id,
          editedTypeDetail.value
        )
      } else {
        const res = await typedetailService.saveTypeDetail(editedTypeDetail.value)
      }

      dialog.value = false
      clearTypeDetail()
      await getTypeDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TypeDetail ได้')
    }
  }

  const editTypeDetail = (typedetail: TypeDetail) => {
    editedTypeDetail.value = { ...typedetail }
    dialog.value = true
  }

  function clearTypeDetail() {
    editedTypeDetail.value = {
      typeDetail_id: 0,
      typeDetail_service: '',
      typeDetail_unitService: '',
      typeDetail_price: 0,
      treatmentType: 0
    }
  }

  return {
    typedetails,
    getTypeDetails,
    deleteTypeDetail,
    saveTypeDetail,
    editTypeDetail,
    editedTypeDetail,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

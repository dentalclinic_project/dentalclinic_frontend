import http from './axios'
import type TreatmentDetail from '@/types/TreatmentDetail'

function getTreatmentDetails() {
  return http.get('/treatment-details')
}

function saveTreatmentDetail(treatmentdetail: TreatmentDetail) {
  return http.post('/treatment-details', treatmentdetail)
}

function updateTreatmentDetail(id: number, treatmentdetail: TreatmentDetail) {
  return http.patch(`/treatment-details/${id}`, treatmentdetail)
}

function deleteTreatmentDetail(id: number) {
  return http.delete(`/treatment-details/${id}`)
}

export default {
  getTreatmentDetails,
  saveTreatmentDetail,
  updateTreatmentDetail,
  deleteTreatmentDetail
}

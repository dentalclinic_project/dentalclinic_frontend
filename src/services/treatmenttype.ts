import type TreatmentType from '@/types/TreatmentType'
import http from './axios'

function getTreatmentTypes() {
  return http.get('/treatment-type')
}

function saveTreatmentType(treatmentType: TreatmentType) {
  return http.post('/treatment-type', treatmentType)
}

function updateTreatmentType(id: number, treatmentType: TreatmentType) {
  return http.patch(`/treatment-type/${id}`, treatmentType)
}

function deleteTreatmentType(id: number) {
  return http.delete(`/treatment-type/${id}`)
}

export default {
  getTreatmentTypes,
  saveTreatmentType,
  updateTreatmentType,
  deleteTreatmentType
}

import http from './axios'
import type Dispense from '@/types/Dispense'

function getDispenses() {
  return http.get('/dispensings')
}

function saveDispense(dispense: Dispense) {
  return http.post('/dispensings', dispense)
}

function updateDispense(id: number, dispense: Dispense) {
  return http.patch(`/dispensings/${id}`, dispense)
}

function deleteDispense(id: number) {
  return http.delete(`/dispensings/${id}`)
}

export default {
  getDispenses,
  saveDispense,
  updateDispense,
  deleteDispense
}

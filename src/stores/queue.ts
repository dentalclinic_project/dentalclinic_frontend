import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type Queue from '@/types/Queue'
import queueService from '@/services/queue'
// import router from "@/router";

export const useQueueStore = defineStore('queue', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedQueue = ref<Queue>({
    que_id: 0,
    que_name: '',
    que_surname: '',
    que_cardnumber: '',
    que_phonenumber: '',
    que_date: '',
    que_time: '',
    patienId: 0,
    treatmenttypeId: 0,
    doctorId: 0
  })

  const queues = ref<Queue[]>([])

  async function getQueues() {
    try {
      const res = await queueService.getQueues()
      queues.value = res.data
      console.log(queues.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Queue ได้')
    }
  }

  async function deleteQueue(id: number) {
    try {
      const res = await queueService.deleteQueue(id)
      await getQueues()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Queue ได้')
    }
  }

  async function saveQueue() {
    try {
      if (editedQueue.value.que_id) {
        const res = await queueService.updateQueue(editedQueue.value.que_id, editedQueue.value)
      } else {
        const res = await queueService.saveQueue(editedQueue.value)
      }

      dialog.value = false
      clearQueue()
      await getQueues()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Queue ได้')
    }
  }

  const editQueue = (queue: Queue) => {
    editedQueue.value = { ...queue }
    dialog.value = true
  }

  function clearQueue() {
    editedQueue.value = {
      que_id: 0,
      que_name: '',
      que_surname: '',
      que_cardnumber: '',
      que_phonenumber: '',
      que_date: '',
      que_time: '',
      patienId: 0,
      treatmenttypeId: 0,
      doctorId: 0
    }
  }

  return {
    queues,
    getQueues,
    deleteQueue,
    saveQueue,
    editQueue,
    editedQueue,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

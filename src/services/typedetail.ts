import http from './axios'
import type TypeDetail from '@/types/TypeDetail'

function getTypeDetails() {
  return http.get('/type-details')
}

function saveTypeDetail(typedetail: TypeDetail) {
  return http.post('/type-details', typedetail)
}

function updateTypeDetail(id: number, typedetail: TypeDetail) {
  return http.patch(`/type-details/${id}`, typedetail)
}

function deleteTypeDetail(id: number) {
  return http.delete(`/type-details/${id}`)
}

export default {
  getTypeDetails,
  saveTypeDetail,
  updateTypeDetail,
  deleteTypeDetail
}

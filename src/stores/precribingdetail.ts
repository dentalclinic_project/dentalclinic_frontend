import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type PrecribingDetail from '@/types/PrecribingDetail'
import precribingdetailService from '@/services/precribingdetail'
// import router from "@/router";

export const usePrecribingDetailStore = defineStore('precribingdetail', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedPrecribingDetail = ref<PrecribingDetail>({
    precribing_detail_id: 0,
    precribing_detail_medicine: '',
    precribing_detail_qty: 0,
    precribing_detail_price: 0,
    meditcineId: 0,
    hisTreatmentId: 0
  })

  const precribingdetails = ref<PrecribingDetail[]>([])

  async function getPrecribingDetails() {
    try {
      const res = await precribingdetailService.getPrecribingDetails()
      precribingdetails.value = res.data
      console.log(precribingdetails.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล PrecribingDetail ได้')
    }
  }

  async function deletePrecribingDetail(id: number) {
    try {
      const res = await precribingdetailService.deletePrecribingDetail(id)
      await getPrecribingDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล PrecribingDetail ได้')
    }
  }

  async function savePrecribingDetail() {
    try {
      if (editedPrecribingDetail.value.precribing_detail_id) {
        const res = await precribingdetailService.updatePrecribingDetail(
          editedPrecribingDetail.value.precribing_detail_id,
          editedPrecribingDetail.value
        )
      } else {
        const res = await precribingdetailService.savePrecribingDetail(editedPrecribingDetail.value)
      }

      dialog.value = false
      clearPrecribingDetail()
      await getPrecribingDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล PrecribingDetail ได้')
    }
  }

  const editPrecribingDetail = (precribingdetail: PrecribingDetail) => {
    editedPrecribingDetail.value = { ...precribingdetail }
    dialog.value = true
  }

  function clearPrecribingDetail() {
    editedPrecribingDetail.value = {
      precribing_detail_id: 0,
      precribing_detail_medicine: '',
      precribing_detail_qty: 0,
      precribing_detail_price: 0,
      meditcineId: 0,
      hisTreatmentId: 0
    }
  }

  return {
    precribingdetails,
    getPrecribingDetails,
    deletePrecribingDetail,
    savePrecribingDetail,
    editPrecribingDetail,
    editedPrecribingDetail,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

export default interface TreatmentDetail {
  treatment_detail_id?: number
  treatment_detail_type: string
  treatment_detail_typedetail: string
  treatment_detail_toothPosition: string
  treatment_detail_price: number
  treatment_detail_qty: number
  treatmenttypeId: number
  typeDetailId: number
  hisTreatmentId: number
}

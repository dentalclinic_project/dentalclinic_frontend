export default interface Patient {
  patien_id?: number
  patien_name: string
  patien_surname: string
  patien_cardnumber: string
  patien_birth: string
  patien_phonenumber: string
  patien_age: number
  patien_gender: string
  patien_address: string
  patien_drugAllergy: string
  patien_congenitalDisease: string
}

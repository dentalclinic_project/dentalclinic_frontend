export default interface Doctor {
  doctor_id?: number
  doctor_name: string
  doctor_surname: string
  doctor_cardnumber: string
  doctor_phonenumber: string
  doctor_position: string
  doctor_address: string
  doctor_gender: string
}

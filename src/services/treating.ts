import http from './axios'
import type Treating from '@/types/Treating'

function getTreatings() {
  return http.get('/his-treatments')
}

function saveTreating(treating: Treating) {
  return http.post('/his-treatments', treating)
}

function updateTreating(id: number, treating: Treating) {
  return http.patch(`/his-treatments/${id}`, treating)
}

function deleteTreating(id: number) {
  return http.delete(`/his-treatments/${id}`)
}

export default {
  getTreatings,
  saveTreating,
  updateTreating,
  deleteTreating
}

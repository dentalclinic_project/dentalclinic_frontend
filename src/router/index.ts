import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Main',
      components: {
        default: () => import('../views/MainView.vue')
      },
      meta: {
        Main: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      components: {
        default: () => import('../views/LoginView.vue')
      },
      meta: {
        Main: true
      }
    },
    {
      path: '/serve',
      name: 'Serve',
      components: {
        default: () => import('../views/ServeView.vue')
      },
      meta: {
        Main: true
      }
    },
    {
      path: '/doctor',
      name: 'Doctor',
      components: {
        default: () => import('../views/DoctorView.vue')
      },
      meta: {
        Main: true
      }
    },
    {
      path: '/empHistoryMedical',
      name: 'Historymedical',
      components: {
        default: () => import('../views/employee/HistoryMedicalView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empPayment',
      name: 'Payment',
      components: {
        default: () => import('../views/employee/PaymentView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empDispense',
      name: 'Dispense',
      components: {
        default: () => import('../views/employee/DispenseView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empAppointment',
      name: 'Appointment',
      components: {
        default: () => import('../views/employee/AppointmentView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empReservationQueue',
      name: 'ReservationQueue',
      components: {
        default: () => import('../views/employee/ReservationQueueView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empPatient',
      name: 'Patient',
      components: {
        default: () => import('../views/employee/patient/PatientView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empManageQueue',
      name: 'ManageQueue',
      components: {
        default: () => import('../views/employee/ManageQueueView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/addPatient',
      name: 'addPatient',
      components: {
        default: () => import('../views/employee/patient/AddPatient.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/editPatient',
      name: 'editPatient',
      components: {
        default: () => import('../views/employee/patient/EditPatient.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empChoosePayment',
      name: 'ChoosePayment',
      components: {
        default: () => import('../views/employee/ChoosePayment.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/empReceiptPayment',
      name: 'ReceiptPayment',
      components: {
        default: () => import('../views/employee/ReceiptPayment.vue')
      },
      meta: {
        employee: true
      }
    },

    // Doctor
    {
      path: '/docTreating',
      name: 'DocTreating',
      components: {
        default: () => import('../views/doctor/TreatingView.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/doctreatment',
      name: 'DocTreatment',
      components: {
        default: () => import('../views/doctor/TreatmentView.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/docTreatmentplan',
      name: 'DocTreatmentplan',
      components: {
        default: () => import('../views/doctor/TreatmentplanView.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/docAddTreatmentplan',
      name: 'DocAddTreatmentplan',
      components: {
        default: () => import('../views/doctor/AddTreatmentPlan.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/docAppointment',
      name: 'DocAppointment',
      components: {
        default: () => import('../views/doctor/AppointmentdocView.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/docPatient',
      name: 'DocPatient',
      components: {
        default: () => import('../views/doctor/PatientdocView.vue')
      },
      meta: {
        doctor: true
      }
    },
    {
      path: '/docDetailTreatment',
      name: 'DocDetailTreatment',
      components: {
        default: () => import('../views/doctor/DetailTreatmentView.vue')
      },
      meta: {
        doctor: true
      }
    }
  ]
})

export default router

import type Medicine from '@/types/Medicine'
import http from './axios'

function getMedicines() {
  return http.get('/medicines')
}

function saveMedicine(medicine: Medicine) {
  return http.post('/medicines', medicine)
}

function updateMedicine(id: number, medicine: Medicine) {
  return http.patch(`/medicines/${id}`, medicine)
}

function deleteMedicine(id: number) {
  return http.delete(`/medicines/${id}`)
}

export default {
  getMedicines,
  saveMedicine,
  updateMedicine,
  deleteMedicine
}

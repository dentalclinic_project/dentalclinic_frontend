import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useMessageStore } from './message'
import { usePatientStore } from './patient'
import type TypeDetail from '@/types/TypeDetail'
import type Medicine from '@/types/Medicine'
import type Patient from '@/types/Patient'
import treatingService from '@/services/treating'

export const useTreatmentStore = defineStore('treatment', () => {
  const patientStore = usePatientStore()
  const num = ref(1)
  const MedicineQty = ref(1)
  const ToothPosition = ref('')
  const ToothPositionList = ref<{ id: number; name: string }[]>([])
  const selectToothPositionList = ref<{ id: number; name: string }[]>([])
  const selectPatient = ref<Patient>({
    patien_id: 0,
    patien_name: '',
    patien_surname: '',
    patien_cardnumber: '',
    patien_birth: '',
    patien_phonenumber: '',
    patien_age: 0,
    patien_gender: '',
    patien_address: '',
    patien_drugAllergy: '',
    patien_congenitalDisease: ''
  })
  const PatientList = ref<Patient[]>([])
  const Tooth = ref<{ id: number; name: string }[]>([
    { id: 1, name: 'T1' },
    { id: 2, name: 'T2' },
    { id: 3, name: 'T3' },
    { id: 4, name: 'T4' },
    { id: 5, name: 'T5' },
    { id: 6, name: 'T6' },
    { id: 7, name: 'T7' },
    { id: 8, name: 'T8' },
    { id: 9, name: 'T9' },
    { id: 10, name: 'T10' },
    { id: 11, name: 'T11' },
    { id: 12, name: 'T12' },
    { id: 13, name: 'T13' },
    { id: 14, name: 'T14' },
    { id: 15, name: 'T15' },
    { id: 16, name: 'T16' },
    { id: 17, name: 'T17' },
    { id: 18, name: 'T18' },
    { id: 19, name: 'T19' },
    { id: 20, name: 'T20' },
    { id: 21, name: 'T21' },
    { id: 22, name: 'T22' },
    { id: 23, name: 'T23' },
    { id: 24, name: 'T24' },
    { id: 25, name: 'T25' },
    { id: 26, name: 'T26' },
    { id: 27, name: 'T27' },
    { id: 28, name: 'T28' },
    { id: 29, name: 'T29' },
    { id: 30, name: 'T30' },
    { id: 31, name: 'T31' },
    { id: 32, name: 'T32' },
    { id: 33, name: 'ALL' }
  ])
  const selectDispense = ref<
    {
      id: number
      name: string
      qty: number
      price: number
      unit: string
      sumPrice: number
    }[]
  >([])

  const TreatmentTypeList = ref<
    {
      id: number
      service: string
      unitService: string
      price: number
      treatmentType: number
      qty: number
      position: string
      num: number
    }[]
  >([])

  const addTypeDetail = (item: TypeDetail) => {
    // for (let i = 0; i < ToothPositionList.value.length; i++) {
    //   if (ToothPosition.value === ""){
    //     ToothPosition.value = ToothPosition.value + ToothPositionList.value[0].name ;
    //   }else{
    //     ToothPosition.value = ToothPosition.value + " ,"+ ToothPositionList.value[0].name ;
    //   }
    //   ToothPositionList.value.splice(0,1);
    // }

    TreatmentTypeList.value.push({
      id: item.typeDetail_id,
      service: item.typeDetail_service,
      unitService: item.typeDetail_unitService,
      price: item.typeDetail_price,
      treatmentType: item.treatmentType,
      qty: 1,
      position: ToothPosition.value,
      num: num.value
    })
    clearToothPositionList()
    num.value += 1
    // if (items) {
    //   // item.qty += 1;
    //   // TreatmentTypeList.value.splice(index, 1, item);
    // } else {
    //   // TreatmentTypeList.value.push({
    //   //   id: item.id,
    //   //   service: item.service,
    //   //   unitService: item.unitService,
    //   //   price: item.price,
    //   //   treatmentType: item.treatmentType,
    //   //   qty: 1,
    //   //   position: "",
    //   // });
    // }
  }

  const addDispense = (item: Medicine) => {
    const items = selectDispense.value.find((_item) => _item.id === item.medicine_id);

    if (items?.id === item.medicine_id) {
      items.qty += 1;
    } else {
      selectDispense.value.push({
        id: item.medicine_id,
        name: item.medicine_name,
        qty: MedicineQty.value,
        price: item.medicine_sell,
        unit: item.medicine_medicineType,
        sumPrice: item.medicine_sell * MedicineQty.value
    })
    
    }
  }

  const selectToothPosition = (item: { id: number; name: string }) => {
    selectToothPositionList.value.push(item)

    ToothPositionList.value.push(item)
    console.log(ToothPositionList.value);
    for (let i = 0; i < ToothPositionList.value.length; i++) {
      if (ToothPosition.value === '') {
        ToothPosition.value = ToothPosition.value + ToothPositionList.value[0].name
      } else {
        ToothPosition.value = ToothPosition.value + ' ,' + ToothPositionList.value[0].name
      }
      ToothPositionList.value.splice(0, 1)
    }
  }

  // const selectPatien = (item: Patient) => {
  //   selectPatient.value.push(item)
  // }

  const editPatient = (patient: Patient) => {
    selectPatient.value = { ...patient }
    PatientList.value.push(selectPatient.value)
  }

  async function saveHisTreatment() {

    const hisTreat = {
      patien_id: 0,
      docter_id: 0 ,
      treatmentDetail: TreatmentTypeList,
      precribingDetail: selectDispense,
    };
    // console.log(order);
    try {
      // const res = await treatingService.saveTreating(hisTreat);
      // await getOrders();
    } catch (e) {
      console.log(e);
    }
  }
  

  const deleteTreatmentTypeList = (item: { id: number; qty: number }): void => {
    const index = TreatmentTypeList.value.findIndex((_item) => _item.id === item.id)
    TreatmentTypeList.value.splice(index, 1)
    item.qty = 1
  }

  const deleteDispenseList = (item: {
    id: number
    name: string
    qty: number
    price: number
    unit: string
    sumPrice: number
  }): void => {
    const index = selectDispense.value.findIndex((_item) => _item.id === item.id)
    selectDispense.value.splice(index, 1)
    item.qty = 1
  }

  function clearToothPositionList() {
    const all = ToothPositionList.value.length
    ToothPositionList.value.splice(0, all)
    ToothPosition.value = ''
  }

  const clearTreatmentTypeList = () => {
    const all = TreatmentTypeList.value.length
    TreatmentTypeList.value.splice(0, all)
  }

  return {
    TreatmentTypeList,
    addTypeDetail,
    deleteTreatmentTypeList,
    Tooth,
    selectToothPosition,
    clearTreatmentTypeList,
    addDispense,
    selectDispense,
    deleteDispenseList,
    editPatient,
    PatientList,selectPatient
  }
})

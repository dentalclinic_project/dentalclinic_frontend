import type PrecribingDetail from '@/types/PrecribingDetail'
import http from './axios'

function getPrecribingDetails() {
  return http.get('/precribing-details')
}

function savePrecribingDetail(precribingdetail: PrecribingDetail) {
  return http.post('/precribing-details', precribingdetail)
}

function updatePrecribingDetail(id: number, precribingdetail: PrecribingDetail) {
  return http.patch(`/precribing-details/${id}`, precribingdetail)
}

function deletePrecribingDetail(id: number) {
  return http.delete(`/precribing-details/${id}`)
}

export default {
  getPrecribingDetails,
  savePrecribingDetail,
  updatePrecribingDetail,
  deletePrecribingDetail
}

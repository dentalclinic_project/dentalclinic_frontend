import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type Dispense from '@/types/Dispense'
import dispenseService from '@/services/dispense'
// import router from "@/router";

export const useDispenseStore = defineStore('dispense', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedDispense = ref<Dispense>({
    dispensing_id: 0,
    dispensing_patien: '',
    dispensing_date: '',
    dispensing_medicine: '',
    dispensing_qty: 0,
    dispensing_price: 0,
    dispensing_counter: '',
    dispensing_doctor: ''
  })

  const dispenses = ref<Dispense[]>([])

  async function getDispenses() {
    try {
      const res = await dispenseService.getDispenses()
      dispenses.value = res.data
      console.log(dispenses.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Dispense ได้')
    }
  }

  async function deleteDispense(id: number) {
    try {
      const res = await dispenseService.deleteDispense(id)
      await getDispenses()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Dispense ได้')
    }
  }

  async function saveDispense() {
    try {
      if (editedDispense.value.dispensing_id) {
        const res = await dispenseService.updateDispense(
          editedDispense.value.dispensing_id,
          editedDispense.value
        )
      } else {
        const res = await dispenseService.saveDispense(editedDispense.value)
      }

      dialog.value = false
      clearDispense()
      await getDispenses()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Dispense ได้')
    }
  }

  const editDispense = (dispense: Dispense) => {
    editedDispense.value = { ...dispense }
    dialog.value = true
  }

  function clearDispense() {
    editedDispense.value = {
      dispensing_id: 0,
      dispensing_patien: '',
      dispensing_date: '',
      dispensing_medicine: '',
      dispensing_qty: 0,
      dispensing_price: 0,
      dispensing_counter: '',
      dispensing_doctor: ''
    }
  }

  return {
    dispenses,
    getDispenses,
    deleteDispense,
    saveDispense,
    editDispense,
    editedDispense,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

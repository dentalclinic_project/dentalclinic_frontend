import type Patient from '@/types/Patient'
import http from './axios'

function getPatients() {
  return http.get('/patiens')
}

function getPat(params: { search: string }) {
  return http.get('/patiens', { params: params })
}

function savePatient(patient: Patient) {
  return http.post('/patiens', patient)
}

function updatePatient(id: number, patient: Patient) {
  return http.patch(`/patiens/${id}`, patient)
}

function deletePatient(id: number) {
  return http.delete(`/patiens/${id}`)
}

export default {
  getPatients,
  savePatient,
  updatePatient,
  deletePatient,
  getPat
}

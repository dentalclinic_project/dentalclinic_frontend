import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import doctorService from '@/services/doctor'
import type Doctor from '@/types/Doctor'
// import router from "@/router";

export const useDoctorStore = defineStore('doctor', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedDoctor = ref<Doctor>({
    doctor_id: 0,
    doctor_name: '',
    doctor_surname: '',
    doctor_cardnumber: '',
    doctor_phonenumber: '',
    doctor_position: '',
    doctor_address: '',
    doctor_gender: ''
  })

  const doctors = ref<Doctor[]>([])

  async function getDoctors() {
    try {
      const res = await doctorService.getDoctors()
      doctors.value = res.data
      console.log(doctors.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Doctor ได้')
    }
  }

  async function deleteDoctor(id: number) {
    try {
      const res = await doctorService.deleteDoctor(id)
      await getDoctors()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Doctor ได้')
    }
  }

  async function saveDoctor() {
    try {
      if (editedDoctor.value.doctor_id) {
        const res = await doctorService.updateDoctor(
          editedDoctor.value.doctor_id,
          editedDoctor.value
        )
      } else {
        const res = await doctorService.saveDoctor(editedDoctor.value)
      }

      dialog.value = false
      clearDoctor()
      await getDoctors()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล Doctor ได้')
    }
  }

  const editDoctor = (doctor: Doctor) => {
    editedDoctor.value = { ...doctor }
    dialog.value = true
  }

  function clearDoctor() {
    editedDoctor.value = {
      doctor_id: 0,
      doctor_name: '',
      doctor_surname: '',
      doctor_cardnumber: '',
      doctor_phonenumber: '',
      doctor_position: '',
      doctor_address: '',
      doctor_gender: ''
    }
  }

  return {
    doctors,
    getDoctors,
    deleteDoctor,
    saveDoctor,
    editDoctor,
    editedDoctor,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

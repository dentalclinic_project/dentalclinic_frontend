import type TreatmentDetail from "./TreatmentDetail"
import type PrecribingDetail from "./PrecribingDetail"

export default interface Treating {
  his_id?: number
  his_patien?: string
  his_date?: string
  his_doctor?: string
  patienId: number
  doctorId: number
  treatmenttypeId?: number
  // treatmentDetailId: number
  // precribingDetailId: number
  treatmentDetail: TreatmentDetail []
  precribingDetail: PrecribingDetail []
}

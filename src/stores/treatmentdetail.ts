import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
// import { useLoadingStore } from "./loading";
import { useMessageStore } from './message'
import type TreatmentDetail from '@/types/TreatmentDetail'
import treatmentdetailService from '@/services/treatmentdetail'
// import router from "@/router";

export const useTreatmentDetailStore = defineStore('treatmentdetail', () => {
  const dialog = ref(false)
  const dialog1 = ref(false)
  const dialog2 = ref(false)
  const isTable = ref(true)
  const messageStore = useMessageStore()

  const editedTreatmentDetail = ref<TreatmentDetail>({
    treatment_detail_id: 0,
    treatment_detail_type: '',
    treatment_detail_typedetail: '',
    treatment_detail_toothPosition: '',
    treatment_detail_price: 0,
    treatment_detail_qty: 0,
    treatmenttypeId: 0,
    typeDetailId: 0,
    hisTreatmentId: 0
  })

  const treatmentdetails = ref<TreatmentDetail[]>([])

  async function getTreatmentDetails() {
    try {
      const res = await treatmentdetailService.getTreatmentDetails()
      treatmentdetails.value = res.data
      console.log(treatmentdetails.value)
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentDetail ได้')
    }
  }

  async function deleteTreatmentDetail(id: number) {
    try {
      const res = await treatmentdetailService.deleteTreatmentDetail(id)
      await getTreatmentDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentDetail ได้')
    }
  }

  async function saveTreatmentDetail() {
    try {
      if (editedTreatmentDetail.value.treatment_detail_id) {
        const res = await treatmentdetailService.updateTreatmentDetail(
          editedTreatmentDetail.value.treatment_detail_id,
          editedTreatmentDetail.value
        )
      } else {
        const res = await treatmentdetailService.saveTreatmentDetail(editedTreatmentDetail.value)
      }

      dialog.value = false
      clearTreatmentDetail()
      await getTreatmentDetails()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถเรียกข้อมูล TreatmentDetail ได้')
    }
  }

  const editTreatmentDetail = (treatmentdetail: TreatmentDetail) => {
    editedTreatmentDetail.value = { ...treatmentdetail }
    dialog.value = true
  }

  function clearTreatmentDetail() {
    editedTreatmentDetail.value = {
      treatment_detail_id: 0,
      treatment_detail_type: '',
      treatment_detail_typedetail: '',
      treatment_detail_toothPosition: '',
      treatment_detail_price: 0,
      treatment_detail_qty: 0,
      treatmenttypeId: 0,
      typeDetailId: 0,
      hisTreatmentId: 0
    }
  }

  return {
    treatmentdetails,
    getTreatmentDetails,
    deleteTreatmentDetail,
    saveTreatmentDetail,
    editTreatmentDetail,
    editedTreatmentDetail,
    isTable,
    dialog,
    dialog1,
    dialog2
  }
})

export default interface Dispense {
  dispensing_id?: number
  dispensing_patien: string
  dispensing_date: string
  dispensing_medicine: string
  dispensing_qty: number
  dispensing_price: number
  dispensing_counter: string
  dispensing_doctor: string
}

export default interface PrecribingDetailDto {
  precribing_detail_id?: number
  precribing_detail_medicine: string
  precribing_detail_qty: number
  precribing_detail_price: number
  meditcineId: number
  hisTreatmentId: number
}

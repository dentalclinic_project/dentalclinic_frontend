export default interface MedicalHistory {
  id?: number
  date: string
  time: string
  name: string
  pill: string
  doctor: string
}

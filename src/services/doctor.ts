import type Doctor from '@/types/Doctor'
import http from './axios'

function getDoctors() {
  return http.get('/doctors')
}

function saveDoctor(doctor: Doctor) {
  return http.post('/doctors', doctor)
}

function updateDoctor(id: number, doctor: Doctor) {
  return http.patch(`/doctors/${id}`, doctor)
}

function deleteDoctor(id: number) {
  return http.delete(`/doctors/${id}`)
}

export default {
  getDoctors,
  saveDoctor,
  updateDoctor,
  deleteDoctor
}

import http from './axios'
import type Queue from '@/types/Queue'

function getQueues() {
  return http.get('/que')
}

function saveQueue(queue: Queue) {
  return http.post('/que', queue)
}

function updateQueue(id: number, queue: Queue) {
  return http.patch(`/que/${id}`, queue)
}

function deleteQueue(id: number) {
  return http.delete(`/que/${id}`)
}

export default {
  getQueues,
  saveQueue,
  updateQueue,
  deleteQueue
}

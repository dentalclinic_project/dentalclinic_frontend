// import type Doctor from './Doctor'
// import type Patient from './Patient'
// import type Treatmenttype from './TreatmentType'

export default interface Queue {
  que_id?: number
  que_name: string
  que_surname: string
  que_cardnumber: string
  que_phonenumber: string
  que_date: string
  que_time: string
  patienId: number
  treatmenttypeId: number
  doctorId: number
}

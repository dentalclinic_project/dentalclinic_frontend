export default interface Medicine {
  medicine_id: number
  medicine_name: string
  medicine_remaining: number
  medicine_cost: number
  medicine_sell: number
  medicine_medicineType: string
  medicine_status: string
  medicineTypeId: number
}
